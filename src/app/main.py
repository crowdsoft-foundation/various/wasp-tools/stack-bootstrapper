#!/usr/bin/env python
from flask import Flask, jsonify
import json
import socketio
from flask_cors import CORS
from waspbootstrapper import WASPBootstrapper
import threading

app = Flask(__name__, static_folder='html', static_url_path='')
sio = None

sio = socketio.Client()
try:
    sio.connect('http://stack-manager.planblick.svc:5000/')
except Exception:
    sio = None

app.config['CORS_SUPPORTS_CREDENTIALS'] = True
CORS(app, resources={r"/*": {"origins": "*"}}, automatic_options=True)

t=None

@app.route('/start-bootstrapping', methods=["GET"])
def start_bootstrapping():
    global t

    t = threading.Thread(name='child procs', target=WASPBootstrapper(sio).bootstrap)
    t.start()

    return jsonify({"message": "OK"}), 200

@app.route('/status', methods=["GET"])
def status():
    return jsonify({"message": "OK"}), 200

@app.route('/healthz', methods=["GET"])
def health_action():
    return jsonify({"message": "OK"}), 200
